
/**
 *
 */
import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import dbConfig from './config/database.config';
import User from './routes/user.route';
import Ads from './routes/ads.route';
import Country from './routes/country.route';
import Review from './routes/review.route';

mongoose.Promise = global.Promise;
// create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// mongodb connection
mongoose.connect(dbConfig.url).then(() => {
  console.log('Successfully connected to the database');
}).catch(err => {
  console.log(`Could not connect to the database. Error:${err}`);
  process.exit();
}); ;

// define a simple route
app.get('/', (req, res) => {
  res.json({message: 'Test'});
});

// connect app routes
User(app);
Ads(app);
Country(app);
Review(app);
// listen for requests
app.listen(8080, () => {
  console.log('Server is listening on port 8080');
});
