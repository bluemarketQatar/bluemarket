import mongoose from 'mongoose';


const Schema = mongoose.Schema({
  name: {
    type: String,
    default: null,
  },
  image: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    default: null,
  },
});

export default mongoose.model('Country', Schema);
