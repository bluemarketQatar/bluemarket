import mongoose from 'mongoose';

/**
 * schema for ads table
 */
const Schema = mongoose.Schema({
  image: {
    type: String,
    default: null,
  },
  title: {
    type: String,
    default: null,
  },
  description: {
    type: String,
    default: null,
  },
  price: { // for single share
    type: Number,
    default: 0,
  },
  shares: { // total number of shares for sale
    type: Number,
    default: 0,
  },
  location: {
    name: {
      type: String,
      default: null,
    },
    lat: {
      type: Number,
      default: null,
    },
    lng: {
      type: Number,
      default: null,
    },
  },
  country: {type: mongoose.Schema.Types.ObjectId, ref: 'Country'}, //
  businessType: {
    type: String,
    default: null,
  },
  percentage: {
    type: String,
    default: null,
  },
  revenue: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    default: null,
  },
  sellerContactInfo: {
    icon: { type: String},
    phone: { type: String},
    email: { types: String},
  },
  postedDate: {
    type: Date,
    default: Date.now,
  },
  tagLine: {
    type: String,
    default: null,
  },
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},

}, {
  timestamps: true,
  autoIndex: true,
});

export default mongoose.model('Ads', Schema);
