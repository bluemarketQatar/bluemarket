import mongoose from 'mongoose';


const Schema = mongoose.Schema({
  ad: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Ads',
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  adViewStatus: {
    type: Boolean,
    default: false,
  },
  adVisitCount: {
    type: Number,
    default: 0,
  },
  adContactViewCount: {
    type: Number,
    default: 0,
  },
});

export default mongoose.model('UserAdsMap', Schema);
