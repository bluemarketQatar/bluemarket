import mongoose from 'mongoose';


const Schema = mongoose.Schema({
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  userType: {
    type: String,
    default: null,
  },
  subject: {
    type: String,
    default: null,
  },
  review: {
    subject: {
      type: String,
      default: null,
    },
    description: {
      type: String,
      default: null,
    },
  },
  postedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
});

export default mongoose.model('Review', Schema);
