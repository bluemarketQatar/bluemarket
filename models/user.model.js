import mongoose from 'mongoose';


/**
 *  schema for user table
 */
const Schema = mongoose.Schema({
  firstName: {
    type: String,
    default: null,
  },
  lastName: {
    type: String,
    default: null,
  },
  middleName: {
    type: String,
    default: null,
  },
  email: {
    type: String,
    default: null,
  },
  phone: {
    type: String,
    default: null,
  },
  country: {type: mongoose.Schema.Types.ObjectId, ref: 'Country'},
  password: {
    type: String,
    default: null,
  },
  image: {
    type: String,
    default: null,
  },
  address: [{
    type: String,
    default: null,
  }],
  loginType: {
    type: String,
    default: 'email',
  },
}, {
  timestamps: true,
  autoIndex: true,
});

export default mongoose.model('User', Schema);
