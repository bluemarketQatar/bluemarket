import Country from '../models/country.model';


const create = (req, res) => {
  const countrys = [{
    name: 'Qatar',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS5Bruf0CXDzI11z_i5lFsrh0LOFbtDQ3scAw06ufBMjrukQulT',
    status: 'active',
  }, {
    name: 'Dubai',
    image: 'https://i.pinimg.com/236x/9b/0d/6f/9b0d6f91b4896a9445636f2394c3f4f7--uae.jpg',
    status: 'active',
  }, {
    name: 'Saudi',
    image: 'https://cdn.britannica.com/79/5779-004-491EE13C.jpg',
    status: 'active',
  }, {
    name: 'India',
    image: 'https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/255px-Flag_of_India.svg.png',
    status: 'active',
  }];
  countrys.forEach(item => {
    const country = new Country(item);
    country.save().then(data => {
      const response = data.toObject();
      delete response.password;
      res.status(200).send(response);
    }).catch(err => {
      res.status(500).send({
        message: err.message ||
          'Some error occurred while creating the country.',
      });
    });
  });
};

const findAll = (req, res) => {
  Country.find({
      status: 'active'
    })
    .then(ads => {
      res.send({
        status: true,
        data: ads,
      });
    }).catch(err => {
      res.status(500).send({
        status: false,
        message: err.message ||
          'Some error occurred while retrieving country list.',
      });
    });
};

export default {
  create,
  findAll,
};
