import UserAdsMap from '../models/adsUserStatus.model';


const create = (req, res) => {
  // Validate request
  if (!req.body) {
    return res.status(400).send({
      status: false,
      message: 'Ads content can not be empty',
    });
  }

  // Create a Ads
  const Status = new UserAdsMap({
    ad: req.body.adsId,
    user: req.body.userId,
    adViewStatus: true,
    adVisitCount: 1,
  });
  UserAdsMap.findOneAndUpdate(
    {
      ad: req.body.adsId,
      user: req.body.userId,
    }, { $inc: { adVisitCount: 1 }}, {new: true}
  ).then(Ads => {
    if (Ads){
      res.send({
        status: true,
        data: Ads,
      });
    } else {
      Status.save()
        .then(data => {
          res.send({
            status: true,
            data: data,
          });
        }).catch(err => {
          res.status(500).send({
            status: false,
            message: err.message ||
              'Some error occurred while creating the Ads.',
          });
        });
    }

  }).catch(err => {
    if (err.kind === 'ObjectId') {
      return res.status(404).send({
        status: false,
        message: 'Ads not found connected to  user-id ' + req.params.userId,
      });
    }
    return res.status(500).send({
      status: false,
      message: 'Error no Ads connected with user-id ' + req.params.userId,
    });
  });
  // Save Ads in the database

};

const update = (req, res) => {
  UserAdsMap.findOneAndUpdate(
    {
      ad: req.body.adsId,
      user: req.body.userId,
    }, { $inc: { adContactViewCount: 1 }}, {new: true}
  ).then(Ads => {
    res.send({
      status: true,
      data: Ads,
    });

  }).catch(err => {
    return res.status(404).send({
      status: false,
      message: err.message,
    });

  });
};

export default {
  create, update,
};
