import Ads from '../models/ads.model';

// Create and Save a new Ads
const create = (req, res) => {
  // Validate request
  if (!req.body) {
    return res.status(400).send({
      status: false,
      message: 'Ads content can not be empty',
    });
  }

  // Create a Ads
  const ads = new Ads(req.body);

  // Save Ads in the database
  ads.save()
    .then(data => {
      res.send({
        status: true,
        data: data,
      });
    }).catch(err => {
      res.status(500).send({
        status: false,
        message: err.message || 'Some error occurred while creating the Ads.',
      });
    });
};

// Retrieve and return all Adss from the database.
const findAll = (req, res) => {
  Ads.find({
    status: 'active',
  })
    .populate('country')
    .populate('user', {
      firstName: 1,
      lastName: 1,
      middleName: 1,
      email: 1,
      phone: 1,
    })
    .then(ads => {
      res.send({
        status: true,
        data: ads,
      });
    }).catch(err => {
      res.status(500).send({
        status: false,
        message: err.message || 'Some error occurred while retrieving Adss.',
      });
    });
};

// Find a single Ads with a adsId
const findOne = (req, res) => {
  Ads.findById(req.params.adsId)
    .then(Ads => {
      if (!Ads) {
        return res.status(404).send({
          status: false,
          message: 'Ads not found with id ' + req.params.adsId,
        });
      }
      res.send({
        status: true,
        data: Ads,
      });
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          status: false,
          message: 'Ads not found with id ' + req.params.adsId,
        });
      }
      return res.status(500).send({
        status: false,
        message: 'Error retrieving Ads with id ' + req.params.adsId,
      });
    });
};

// Update a Ads identified by the adsId in the request
const update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      status: false,
      message: 'Ads content can not be empty',
    });
  }

  // Find Ads and update it with the request body
  Ads.findByIdAndUpdate(req.params.adsId, req.body, {
    new: true,
  })
    .populate('country')
    .populate('user', {
      firstName: 1,
      lastName: 1,
      middleName: 1,
      email: 1,
      phone: 1,
    })
    .then(Ads => {
      if (!Ads) {
        return res.status(404).send({
          status: false,
          message: 'Ads not found with id ' + req.params.adsId,
        });
      }
      res.send({
        status: true,
        data: Ads,
      });
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          status: false,
          message: 'Ads not found with id ' + req.params.adsId,
        });
      }
      return res.status(500).send({
        status: false,
        message: 'Error updating Ads with id ' + req.params.adsId,
      });
    });
};

// Delete a Ads with the specified adsId in the request
const remove = (req, res) => {
  Ads.findByIdAndRemove(req.params.adsId)
    .then(Ads => {
      if (!Ads) {
        return res.status(404).send({
          status: false,
          message: 'Ads not found with id ' + req.params.adsId,
        });
      }
      res.send({
        status: true,
        message: 'Ads deleted successfully!',
      });
    }).catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          status: false,
          message: 'Ads not found with id ' + req.params.adsId,
        });
      }
      return res.status(500).send({
        status: false,
        message: 'Could not delete Ads with id ' + req.params.adsId,
      });
    });
};

/**
 * findAllByUser - description
 *
 * @param  {type} req description
 * @param  {type} res description
 * @return {type}     description
 */
const findAllByUser = (req, res) => {
  Ads.findOne({
    user: req.params.userId,
  })
    .populate('country')
    .populate('user', {
      firstName: 1,
      lastName: 1,
      middleName: 1,
      email: 1,
      phone: 1,
    })
    .then(Ads => {
      if (!Ads) {
        return res.status(404).send({
          status: false,
          message: 'Ads not found with user ' + req.params.userId,
        });
      }
      res.send({
        status: true,
        data: Ads,
      });
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          status: false,
          message: 'Ads not found connected to  user-id ' + req.params.userId,
        });
      }
      return res.status(500).send({
        status: false,
        message: 'Error no Ads connected with user-id ' + req.params.userId,
      });
    });
};


export default {
  create,
  findAll,
  findOne,
  update,
  remove,
  findAllByUser,
};
