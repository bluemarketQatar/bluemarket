import Review from '../models/review.model';

const create = (req, res) => {
  // Validate request
  if (!req.body) {
    return res.status(400).send({
      status: false,
      message: 'Ads content can not be empty',
    });
  }

  // Create a Ads
  const review = new Review(req.body);

  // Save Ads in the database
  review.save()
    .then(data => {
      Review.findOne(data)
        .populate('user', {firstName: 1, lastName: 1,
          middleName: 1, email: 1, phone: 1})
        .populate('postedBy', {firstName: 1, lastName: 1,
          middleName: 1, email: 1, phone: 1})
        .exec((err, item) => {
          if (err){
            res.status(500).send({
              status: false,
              message: err.message ||
            'Some error occurred while creating the review.',
            });
          } else {
            res.send({
              status: true,
              message: 'success',
              data: item,
            }
            );
          }
        });

    }).catch(err => {
      res.status(500).send({
        status: false,
        message: err.message ||
        'Some error occurred while creating the review.',
      });
    });
};

export default { create };
