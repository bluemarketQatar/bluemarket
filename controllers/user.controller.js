import Crypto from 'crypto';
import User from '../models/user.model';

const algorithm = 'aes-128-cbc';
const password = 'd6F3Efeq';

function encrypt(text){
  var cipher = Crypto.createCipher(algorithm, password);
  var crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}


/**
 * create and save user
 */
const register = (req, res) => {

  if (!req.body) {
    return res.status(400).send({
      message: 'User content can not be empty',
    });
  }

  const user = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName || null,
    middleName: req.body.middleName || null,
    email: req.body.email,
    phone: req.body.phone || null,
    password: encrypt(req.body.password),
    address: req.body.address || null,
    country: req.body.country || null,
  });

  // checking user with same email id already in collection
  User.find({email: req.body.email}, (error, users) => {

    if (error){
      res.status(500).send({
        message: error.message,
      });
    }
    // if user with email doesnt exisit
    if (users.length === 0){
      // Save User in the database
      user.save()
        .then(data => {
          User.findOne(data).populate('country').exec((err, item) => {
            if (err){
              res.status(500).send({
                status: false,
                message: err.message ||
                 'Some error occurred while creating the User.',
              });
            } else {
              const response = item.toObject();
              delete response.password;
              res.status(200).send({
                status: true, data: response,
              });
            }

          });

        }).catch(err => {
          res.status(500).send({
            status: false,
            message: err.message ||
             'Some error occurred while creating the User.',
          });
        });
    } else {
      res.status(500).send({
        status: false,
        message: 'This email address already registered',
      });
    }
  });


};


/**
 * login for user with valid email id and password
 */
const login = (req, res) => {
  User.findOne({email: req.body.email, password: encrypt(req.body.password)})
    .populate('country')
    .exec((error, user) => {
      if (error){
        res.status(404).send({
          status: false,
          message: error.message,
        });
      }
      if (!user){
        res.status(404).send({
          status: false,
          message: 'Please check your login credentials',
        });
      } else {
        const response = user.toObject();
        delete response.password;
        res.status(200).send({
          status: true, data: response,
        });
      }
    });
};


/**
 * Retrieve and return all Users from the database.
 */
const findAll = (req, res) => {
  User.find()
    .then(users => {
      res.send({
        status: true, data: users,
      });
    }).catch(err => {
      res.status(500).send({
        status: false,
        message: err.message || 'Some error occurred while retrieving Users.',
      });
    });
};


/**
 * Find a single User with a userId
 */
const findOne = (req, res) => {
  User.findById(req.params.userId)
    .then(User => {
      if (!User) {
        return res.status(404).send({
          status: false,
          message: 'User not found with id ' + req.params.userId,
        });
      }
      res.send({
        status: true, data: User,
      });
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          status: false,
          message: 'User not found with id ' + req.params.userId,
        });
      }
      return res.status(500).send({
        status: false,
        message: 'Error retrieving User with id ' + req.params.userId,
      });
    });
};


/**
 * Update a User identified by the userId in the request
 */
const update = (req, res) => {
  // Validate Request
  if (!req.body.content) {
    return res.status(400).send({
      status: false,
      message: 'User content can not be empty',
    });
  }

  // Find User and update it with the request body
  User.findByIdAndUpdate(req.params.userId, req.body, {new: true})
    .then(user => {
      if (!user) {
        return res.status(404).send({
          status: false,
          message: 'User not found with id ' + req.params.userId,
        });
      }
      res.send({
        status: true, data: user,
      });
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          status: false,
          message: 'User not found with id ' + req.params.userId,
        });
      }
      return res.status(500).send({
        status: false,
        message: 'Error updating User with id ' + req.params.userId,
      });
    });
};


/**
 * Delete a User with the specified userId in the request
 */
const remove = (req, res) => {
  User.findByIdAndRemove(req.params.userId)
    .then(User => {
      if (!User) {
        return res.status(404).send({
          status: false,
          message: 'User not found with id ' + req.params.userId,
        });
      }
      res.send({status: true, message: 'User deleted successfully!'});
    }).catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          status: false,
          message: 'User not found with id ' + req.params.userId,
        });
      }
      return res.status(500).send({
        status: false,
        message: 'Could not delete User with id ' + req.params.userId,
      });
    });
};

export default {
  register, login, findAll, findOne, update, remove,
};
