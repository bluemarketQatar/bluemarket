
import controller from '../controllers/user.controller';

const routeUser = (app) => {

  const users = controller;
  // Create a new Users
  app.post('/register/', users.register);

  // login for a user
  app.post('/login/', users.login);
  // Retrieve all Userss
  app.get('/users', users.findAll);

  // Retrieve a single Users with UsersId
  app.get('/users/:userId', users.findOne);

  // Update a Users with UsersId
  app.put('/users/:userId', users.update);

  // Delete a Users with UsersId
  app.delete('/users/:userId', users.remove);
};

export default routeUser;
