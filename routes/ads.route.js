
import controller from '../controllers/ads.controller';
import statusController from '../controllers/status.controller';

const routeUser = (app) => {

  const ads = controller;
  const status = statusController;
  // Create a new Ads
  app.post('/ads', ads.create);

  // Retrieve all Adss
  app.get('/ads', ads.findAll);

  // get all ads by a user
  app.get('/ads/user/:userId', ads.findAllByUser);

  // Retrieve a single Ads with AdsId
  app.get('/ads/:adsId', ads.findOne);

  // Update a Ads with AdsId
  app.put('/ads/:adsId', ads.update);

  // Delete a Ads with AdsId
  app.delete('/ads/:adsId', ads.remove);
  //
  app.post('/ads/viewStatus', status.create);
  //
  app.post('/ads/contactViewStatus', status.update);
};

export default routeUser;
