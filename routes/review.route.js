
import controller from '../controllers/review.controller';

const routeUser = (app) => {

  const review = controller;
  // Create a new Ads
  app.post('/review', review.create);

};

export default routeUser;
