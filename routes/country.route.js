import controller from '../controllers/country.controller';


const Route = (app) => {
  const country = controller;
  // Create a new Ads
  app.post('/country', country.create);
  // to get complete country list
  app.get('/country', country.findAll);
};
export default Route;
